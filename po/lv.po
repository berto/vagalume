# translation of lv.po to Latvian
# vagalume.pot - Master translation file
# Copyright (C) 2007,2008 Alberto Garcia Gonzalez
# This file is part of Vagalume and is published under the GNU GPLv3
# See the README file for more details.
# Pēteris Caune <cuu508@gmail.com>, 2008, 2010, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: lv\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-10-28 20:38+0200\n"
"PO-Revision-Date: 2013-10-28 20:40+0200\n"
"Last-Translator: Pēteris Caune <cuu508@gmail.com>\n"
"Language-Team: Latvian <en@li.org>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#: ../data/vagalume.desktop.in.in.h:1
msgid "Last.fm Client"
msgstr "Last.fm klients"

#: ../data/vagalume.desktop.in.in.h:2
msgid "Vagalume Last.fm Client"
msgstr "Vagalume Last.fm klients"

#: ../src/connection.c:58
msgid "Error initializing internet connection manager"
msgstr "Kļūda inicializējot tīkla savienojumu pārvaldnieku"

#: ../src/controller.c:493
msgid "Server info imported correctly"
msgstr "Serveru informācija veiksmīgi nolasīta"

#: ../src/controller.c:497
msgid "No server info found in that file"
msgstr "Šajā failā netika atrasta serveru informācija"

#: ../src/controller.c:543
#, c-format
msgid ""
"Unable to login to %s\n"
"Check username and password"
msgstr ""
"Neizdodas pieslēgties %s\n"
"Pārbaudiet lietotājvārdu un paroli"

#: ../src/controller.c:549
msgid "Network connection error"
msgstr "Tīkla savienojuma kļūda"

#: ../src/controller.c:655
msgid ""
"You need to enter your\n"
"username and password\n"
"to be able to use this program."
msgstr ""
"Nepieciešams norādīt\n"
"lietotājvārdu un paroli lai varētu lietot\n"
"šo programmu."

#: ../src/controller.c:717
msgid "No more content to play"
msgstr "Nav vairāk ko spēlēt."

#: ../src/controller.c:945
#, c-format
msgid "Downloaded %s - %s"
msgstr "Lejupielādēts %s - %s"

#: ../src/controller.c:949
#, c-format
msgid "Error downloading %s - %s"
msgstr "Kļūda lejupielādējot %s - %s"

#: ../src/controller.c:989
msgid "Track already being downloaded"
msgstr "Ieraksts jau tiek lejupielādēts"

#: ../src/controller.c:994
msgid "Download this track?"
msgstr "Lejupielādēt šo ierakstu?"

#: ../src/controller.c:1007
msgid "File exists. Overwrite?"
msgstr "Fails eksistē. Pārrakstīt?"

#: ../src/controller.c:1023
#, c-format
msgid "Downloading %s - %s"
msgstr "Lejupielādē %s - %s"

#: ../src/controller.c:1065
msgid "Artist added to bookmarks"
msgstr "Pievienots grāmatzīmēm"

#: ../src/controller.c:1072
msgid "Track added to bookmarks"
msgstr "Ieraksts pievienots grāmatzīmēm"

#: ../src/controller.c:1078
msgid "Current radio added to bookmarks"
msgstr "Pašreizējais radio pievienots grāmatzīmēm"

#: ../src/controller.c:1081
msgid "Bookmark added"
msgstr "Grāmatzīme pievienota"

#: ../src/controller.c:1167
msgid "Really mark track as loved?"
msgstr "Tiešām atzīmēt ierakstu kā mīļu?"

#: ../src/controller.c:1171
msgid "Marking track as loved"
msgstr "Atzīmē ierakstu kā mīļu"

#: ../src/controller.c:1185
msgid "Really ban this track?"
msgstr "Tiešām aizliegt šo ierakstu?"

#: ../src/controller.c:1189
msgid "Banning track"
msgstr "Aizliedz ierakstu"

#: ../src/controller.c:1228
msgid "Tags set correctly"
msgstr "Tagi uzstādīti korekti"

#: ../src/controller.c:1228
msgid "Error tagging"
msgstr "Kļūda uzstādot tagus"

#: ../src/controller.c:1272
msgid "You must type a list of tags"
msgstr "Nepieciešams ievadīt tagu sarakstu"

#: ../src/controller.c:1301
msgid "Recommendation sent"
msgstr "Rekomendācija nosūtīta"

#: ../src/controller.c:1302
msgid "Error sending recommendation"
msgstr "Kļūda sūtot rekomendāciju"

#: ../src/controller.c:1346
msgid ""
"You must type a user name\n"
"and a recommendation message."
msgstr ""
"Nepieciešams norādīt lietotājvārdu\n"
"un rekomendācijas ziņu."

#: ../src/controller.c:1369
msgid "Track added to playlist"
msgstr "Ieraksts pievienots repertuāram"

#: ../src/controller.c:1370
msgid "Error adding track to playlist"
msgstr "Kļūda pievienojot ierakstu repertuāram"

#: ../src/controller.c:1418
msgid "Really add this track to the playlist?"
msgstr "Tiešām pievienot šo ierakstu repertuāram?"

#: ../src/controller.c:1450
msgid ""
"This radio is not available\n"
"in your country."
msgstr ""

#: ../src/controller.c:1454
msgid ""
"Invalid radio URL. Either\n"
"this radio doesn't exist\n"
"or it is only available\n"
"for Last.fm subscribers"
msgstr ""
"Nederīga radio adrese. Vai nu\n"
"šī radio stacija neeksistē,\n"
"vai arī tā ir pieejama tikai\n"
"Last.fm abonentiem"

#: ../src/controller.c:1557 ../src/controller.c:1698 ../src/controller.c:1752
msgid "Enter tag"
msgstr "Ievadiet tagu"

#: ../src/controller.c:1557
msgid "Enter one of your tags"
msgstr "Ievadiet kādu no saviem tagiem"

#: ../src/controller.c:1609
msgid "Enter user name"
msgstr "Ievadiet lietotājvārdu"

#: ../src/controller.c:1610
msgid "Play this user's radio"
msgstr "Spēlēt šī lietotāja radio"

#: ../src/controller.c:1698
msgid "Enter a tag"
msgstr "Ievadiet tagu"

#: ../src/controller.c:1729
msgid "Enter group"
msgstr "Ievadiet grupu"

#: ../src/controller.c:1729
msgid "Enter group name"
msgstr "Ievadiet grupas vārdu"

#: ../src/controller.c:1752
msgid "Enter a global tag"
msgstr "Ievadiet globālo tagu"

#: ../src/controller.c:1776
msgid "Enter artist"
msgstr "Ievadiet izpildītāju"

#: ../src/controller.c:1776
msgid "Enter an artist's name"
msgstr "Ievadiet izpildītāja vārdu"

#: ../src/controller.c:1798
msgid "Enter radio URL"
msgstr "Ievadiet radio adresi"

#: ../src/controller.c:1799
msgid "Enter the URL of the Last.fm radio"
msgstr "Ievadiet Last.fm radio adresi"

#: ../src/controller.c:1808 ../src/uimisc.c:312
msgid "Last.fm radio URLs must start with lastfm://"
msgstr "Last.fm radio adresēm jāsākas ar lastfm://"

#: ../src/controller.c:1879
msgid "Unable to initialize DBUS"
msgstr "Neizdevās inicializēt DBUS"

#: ../src/controller.c:1912
msgid "Error reading the server list file"
msgstr "Kļūda lasot serveru saraksta failu"

#: ../src/controller.c:1920
msgid "Error initializing audio system"
msgstr "Neizdevās inicializēt audio sistēmu"

#: ../src/controller.c:1930
msgid "Unable to initialize OSSO context"
msgstr "Neizdevās inicializēt OSSO kontekstu"

#: ../src/dlwin.c:96
msgid "Download complete!"
msgstr "Lejupielāde pabeigta!"

#: ../src/dlwin.c:97
msgid "Download error!"
msgstr "Lejupielādes kļūda!"

#: ../src/dlwin.c:99
msgid "C_lose"
msgstr "A_izvērt"

#: ../src/dlwin.c:155
#, c-format
msgid "Downloading %s"
msgstr "Lejupielādē %s"

#: ../src/dlwin.c:158
#, c-format
msgid ""
"Downloading file\n"
"%s"
msgstr ""
"Lejupielādē failu\n"
"%s"

#: ../src/dlwin.c:163
msgid "_Cancel"
msgstr "_Atcelt"

#: ../src/main.c:85
#, c-format
msgid ""
"Usage:\n"
"  %s [-d decoder] [-s sink] [lastfm radio url]\n"
"\n"
"  decoder:   GStreamer decoder (default: '%s')\n"
"  sink:      GStreamer sink (default: '%s')\n"
msgstr ""
"Lietošana: \n"
"  %s [-d decoder] [-s sink] [lastfm radio adrese]\n"
"\n"
"  decoder:   GStreamer dekoderis (noklusētais: '%s')\n"
"  sink:      GStreamer mērķis (noklusētais: '%s')\n"

#: ../src/main.c:98
#, c-format
msgid "Too many unrecognized options\n"
msgstr "Pārāk daudz neatpazītu parametru\n"

#: ../src/main.c:107
#, c-format
msgid "Usage: %s [lastfm radio url]\n"
msgstr "Lietošana: %s [lastfm radio adrese]\n"

#: ../src/protocol.c:246
msgid "(unknown radio)"
msgstr "(nezināms radio)"

#: ../src/uimisc-gtk.c:119
msgid "User tag radio"
msgstr "Lietotāja iezīmes radio"

#: ../src/uimisc-gtk.c:120 ../src/uimisc-gtk.c:240
msgid "Username:"
msgstr "Lietotājvārds:"

#: ../src/uimisc-gtk.c:121
msgid "Tag:"
msgstr "Pievienot iezīmi..."

#: ../src/uimisc-gtk.c:214
msgid "No help available"
msgstr "Palīdzība nav pieejama"

#: ../src/uimisc-gtk.c:241
msgid "Password:"
msgstr "Parole:"

#: ../src/uimisc-gtk.c:242
msgid "Service:"
msgstr "Serviss:"

#: ../src/uimisc-gtk.c:243
msgid "Enable scrobbling:"
msgstr "Aktivizēt atskaņoto dziemu reģistrēšanu:"

#: ../src/uimisc-gtk.c:244
msgid "Discovery mode:"
msgstr "Atklājumu režīms:"

#. Account frame
#: ../src/uimisc-gtk.c:291 ../src/uimisc-hildon22.c:271
msgid "Account"
msgstr "Konts"

#. Set help
#: ../src/uimisc-gtk.c:294
msgid ""
"* Username and password:\n"
"From your account.\n"
"\n"
"* Service:\n"
"Last.fm-compatible service to connect to.\n"
"\n"
"* Scrobbling:\n"
"Display the music that you listen to on your profile.\n"
"\n"
"* Discovery mode:\n"
"Don't play music you've already listened to. Requires a Last.fm subscription."
msgstr ""
"* Lietotājvārds un parole:\n"
"No jūsu konta.\n"
"\n"
"* Serviss:\n"
"Ar Last.fm savietojams serviss.\n"
"\n"
"* Dziesmu reģistrēšana:\n"
"Rādīt noklausītās dziesmas jūsu profilā.\n"
"\n"
"* Atklājumu režīms:\n"
"Nespēlēt mūziku, kas jau ir dzirdēta. Nepieciešams Last.fm abonements."

#: ../src/uimisc-gtk.c:328
msgid "Use proxy"
msgstr "Lietot starpniekserveri"

#: ../src/uimisc-gtk.c:329
msgid "Proxy address:"
msgstr "Starpniekservera adrese:"

#: ../src/uimisc-gtk.c:330 ../src/uimisc-hildon22.c:346
msgid "Low bitrate stream"
msgstr "Zema bitu ātruma plūsma"

#: ../src/uimisc-gtk.c:352
msgid ""
"* Use proxy:\n"
"Enable this to use a proxy.\n"
"\n"
msgstr ""
"* Lietot starpniekserveri:\n"
"Aktivizējiet, lai lietot starpniekserveri.\n"
"\n"

#: ../src/uimisc-gtk.c:355
msgid "Use system-wide proxy"
msgstr "Lietot sistēmā definēto starpniekserveri"

#: ../src/uimisc-gtk.c:369
msgid ""
"* Use system-wide proxy:\n"
"Use the system-wide proxy configuration.\n"
"\n"
msgstr ""
"* Lietot sistēmā definēto starpniekserveri:\n"
"Lietot sistēmā definēto starpniekservera konfigurāciju.\n"
"\n"

#. Connection frame
#: ../src/uimisc-gtk.c:380 ../src/uimisc-hildon22.c:323
msgid "Connection"
msgstr "Savienojums"

#: ../src/uimisc-gtk.c:385
msgid ""
"* Proxy address:\n"
"[protocol://]user:password@hostname:port\n"
"Only the hostname (can be an IP address) is required.\n"
"Supported protocols: http, socks4, socks5.\n"
"\n"
"* Low bitrate stream:\n"
"Try to get a low bitrate stream from the server\n"
"to save bandwidth."
msgstr ""
"* Starpniekservera adrese:\n"
"[protokols://]lietotājs:parole@resursdators:ports\n"
"Nepieciešams tikai resursdators (var būt IP adrese).\n"
"Uzturētie protokoli: http, socks4, socks5.\n"
"\n"
"* Zema bitu ātruma plūsma:\n"
"mēģināt izmantot zema bitu ātruma datu plūsmu lai taupītu joslas platumu."

#: ../src/uimisc-gtk.c:408 ../src/uimisc.c:399
msgid "Select download directory"
msgstr "Izvēlieties lejupielāžu mapi"

#. Translators: keep this string short!!
#: ../src/uimisc-gtk.c:409 ../src/uimisc-hildon22.c:370
msgid "Automatically download free tracks"
msgstr "Automātiski lejupielādēt brīvos ierakstus"

#. Download frame
#: ../src/uimisc-gtk.c:432 ../src/uimisc-hildon22.c:350
msgid "Download"
msgstr "Lejupielādēt"

#. Set help
#: ../src/uimisc-gtk.c:435
msgid ""
"* Download directory:\n"
"Where to download mp3 files. Note that you can only download those songs "
"marked as freely downloadable by the server."
msgstr ""
"* Lejupielādes mape:\n"
"Kur saglabāt mp3 failus. Ievērojiet, ka saglabāt var tikai tās dziesmas, "
"kuras atzīmētas kā brīvi lejupielādējamas."

#: ../src/uimisc-gtk.c:454
msgid "IM message template:"
msgstr "IM ziņas sagatave:"

#: ../src/uimisc-gtk.c:455
msgid "Update Pidgin status:"
msgstr "Atjaunināt Pidgin statusu:"

#: ../src/uimisc-gtk.c:456
msgid "Update Gajim status:"
msgstr "Atjaunināt Gajim statusu:"

#: ../src/uimisc-gtk.c:457
msgid "Update Gossip status:"
msgstr "Atjaunināt Gossip statusu:"

#: ../src/uimisc-gtk.c:458
msgid "Update Telepathy status:"
msgstr "Atjaunināt Telepathy statusu:"

#. IM status frame
#: ../src/uimisc-gtk.c:489 ../src/uimisc-hildon22.c:374
#: ../src/uimisc-hildon22.c:385
msgid "Update IM status"
msgstr "Atjaunināt IM statusu"

#. Set help
#: ../src/uimisc-gtk.c:492
msgid ""
"Vagalume can update the status message of your IM client each time a new "
"song starts. The template can contain the following keywords:\n"
"\n"
"{artist}: Name of the artist\n"
"{title}: Song title\n"
"{station}: Radio station\n"
"{version}: Vagalume version"
msgstr ""
"Vagalume var atjaunināt statusa ziņu IM klientā katru reizi, kad sākas jauna "
"dziesma. Sagatave var saturēt šādus atslēgvārdus:\n"
"\n"
"{artist}: Izpildītāja vārds\n"
"{title}: Dziesmas nosaukums\n"
"{station}: Radio stacija\n"
"{version}: Vagalume versija"

#: ../src/uimisc-gtk.c:522
msgid "Disable confirmation dialogs:"
msgstr "Atslēgt apstiprinājuma dialogus:"

#: ../src/uimisc-gtk.c:532
msgid ""
"* Disable confirmation dialogs:\n"
"Don't ask for confirmation when loving/banning tracks."
msgstr ""
"* Atslēgt apstiprinājuma dialogus:\n"
"Nejautāt apstiprinājumu atzīmējot dziesmu kā mīļu vai to aizliedzot."

#: ../src/uimisc-gtk.c:539
msgid "Show playback notifications:"
msgstr "Rādīt atskaņošanas ziņas:"

#: ../src/uimisc-gtk.c:549
msgid ""
"\n"
"\n"
"* Show playback notifications:\n"
"Pop up a notification box each time a new song starts.\n"
"\n"
msgstr ""
"\n"
"\n"
"* Rādīt atskaņošanas ziņas:\n"
"Parāda darbvirsmā lodziņu katru reizi, kad sākas jauna dziesma.\n"
"\n"

#: ../src/uimisc-gtk.c:555
msgid "Close to systray:"
msgstr "Aizvērt uz sistēmas ikonu joslu:"

#: ../src/uimisc-gtk.c:565
msgid ""
"* Close to systray:\n"
"Hide the Vagalume window when the close button is pressed."
msgstr ""
"* Aizvērt uz sistēmas ikonu joslu:\n"
"Paslēpj Vagalume logu, kad tiek nospiesta aizvēršanas poga."

#. Misc frame
#: ../src/uimisc-gtk.c:572 ../src/uimisc-hildon22.c:403
msgid "Misc"
msgstr "Dažādi"

#. Create dialog
#: ../src/uimisc-gtk.c:591 ../src/uimisc-hildon22.c:258
msgid "User settings"
msgstr "Lietotāja uzstādījumi"

#: ../src/uimisc-gtk.c:717 ../src/uimisc-hildon22.c:566
msgid "Artist: "
msgstr "Izpildītājs:"

#: ../src/uimisc-gtk.c:725 ../src/uimisc-hildon22.c:570
msgid "Track: "
msgstr "Ieraksts:"

#: ../src/uimisc-gtk.c:734 ../src/uimisc-hildon22.c:575
msgid "Album: "
msgstr "Albums :"

#. Dialog and basic settings
#: ../src/uimisc-gtk.c:787 ../src/uimisc-hildon22.c:657
msgid "Send a recommendation"
msgstr "Nosūtīt rekomendāciju"

#: ../src/uimisc-gtk.c:794 ../src/uimisc-hildon22.c:664
msgid "Recommend this"
msgstr "Rekomendēt šo"

#: ../src/uimisc-gtk.c:817 ../src/uimisc-hildon22.c:668
msgid "Send recommendation to"
msgstr "Nosūtīt rekomendāciju"

#. Message of the recommendation
#: ../src/uimisc-gtk.c:825 ../src/uimisc-hildon22.c:675
msgid "Recommendation message"
msgstr "Rekomendācijas ziņa"

#: ../src/uimisc-gtk.c:1003 ../src/uimisc-gtk.c:1070
#: ../src/uimisc-hildon22.c:848 ../src/uimisc-hildon22.c:850
msgid "retrieving..."
msgstr "saņem..."

#. A treemodel for combos with no elements
#: ../src/uimisc-gtk.c:1067
msgid "(none)"
msgstr "(nekas)"

#. Dialog and basic settings
#: ../src/uimisc-gtk.c:1075 ../src/uimisc-hildon22.c:967
msgid "Tagging"
msgstr "Tagi"

#. Combo to select what to tag
#: ../src/uimisc-gtk.c:1081 ../src/uimisc-hildon22.c:973
msgid "Tag this"
msgstr "Pievienot šim tagu"

#. Text entry
#: ../src/uimisc-gtk.c:1101
msgid ""
"Enter a comma-separated\n"
"list of tags"
msgstr ""
"Ievadiet ar komatiem \n"
"atdalītu sarakstu ar tagiem"

#. Combo boxes
#: ../src/uimisc-gtk.c:1107 ../src/uimisc-hildon22.c:981
msgid "Your favourite tags"
msgstr "Jūsu iecienītākie tagi"

#: ../src/uimisc-gtk.c:1108 ../src/uimisc-hildon22.c:989
msgid "Popular tags for this"
msgstr "Populāri šī ieraksta tagi"

#: ../src/uimisc-hildon22.c:278
msgid "Username"
msgstr "Lietotājvārds"

#: ../src/uimisc-hildon22.c:289
msgid "Password"
msgstr "Parole"

#: ../src/uimisc-hildon22.c:306
msgid "Service"
msgstr "Serviss"

#: ../src/uimisc-hildon22.c:314
msgid "Enable scrobbling"
msgstr "Aktivizēt atskaņoto dziemu reģistrēšanu"

#: ../src/uimisc-hildon22.c:319
msgid "Discovery mode"
msgstr "Atklājumu režīms"

#: ../src/uimisc-hildon22.c:330
msgid "Use HTTP proxy"
msgstr "Lietot HTTP starpniekserveri"

#: ../src/uimisc-hildon22.c:335
msgid "Proxy address"
msgstr "Starpniekservera adrese"

#: ../src/uimisc-hildon22.c:358
msgid "Download directory"
msgstr "Lejupielāžu mape"

#: ../src/uimisc-hildon22.c:393
msgid "IM message template"
msgstr "IM ziņas sagatave"

#: ../src/uimisc-hildon22.c:411
msgid "Disable confirmation dialogs"
msgstr "Atslēgt apstiprinājuma dialogus"

#: ../src/uimisc-hildon22.c:911 ../src/uimisc-hildon22.c:939
#, c-format
msgid "%d tags"
msgstr "%d tagi"

#. Text entry
#: ../src/uimisc-hildon22.c:976
msgid "Enter a comma-separated list of tags"
msgstr "Ievadiet ar komatiem atdalītu sarakstu ar tagiem"

#: ../src/uimisc.c:57
msgid "Client for Last.fm and compatible services"
msgstr "Last.fm un savietojamu servisu klients"

#: ../src/uimisc.c:172
msgid "Catalan"
msgstr "Katalāņu"

#: ../src/uimisc.c:173
msgid "German"
msgstr "Vācu"

#: ../src/uimisc.c:173
msgid "Spanish"
msgstr "Spāņu"

#: ../src/uimisc.c:174
msgid "Finnish"
msgstr "Somu"

#: ../src/uimisc.c:174
msgid "French"
msgstr "Franču"

#: ../src/uimisc.c:175
msgid "Galician"
msgstr "Galisiešu"

#: ../src/uimisc.c:175
msgid "Italian"
msgstr "Itāļu"

#: ../src/uimisc.c:176
msgid "Latvian"
msgstr "Latviešu"

#: ../src/uimisc.c:176
msgid "Polish"
msgstr "Poļu"

#: ../src/uimisc.c:177
msgid "Portuguese"
msgstr "Portugāļu"

#: ../src/uimisc.c:178
msgid "Russian"
msgstr "Krievu"

#: ../src/uimisc.c:223
msgid "Confirmation"
msgstr "Apstiprinājums"

#: ../src/uimisc.c:276
msgid "Add bookmark"
msgstr "Pievienot grāmatzīmi"

#: ../src/uimisc.c:276
msgid "Edit bookmark"
msgstr "Labot grāmatzīmi"

#: ../src/uimisc.c:277
msgid "Bookmark name"
msgstr "Grāmatzīmes nosaukums"

#: ../src/uimisc.c:278
msgid "Last.fm radio address"
msgstr "Last.fm radio adrese"

#: ../src/uimisc.c:308
msgid "Invalid bookmark name"
msgstr "Nederīgs grāmatzīmes nosaukums"

#: ../src/uimisc.c:383
msgid "Select server file to import"
msgstr "Izvēlieties serveru failu importēšanai"

#. Create all widgets
#: ../src/uimisc.c:478
msgid "Stop automatically"
msgstr "Apturēt atskaņošanu automātiski"

#: ../src/uimisc.c:479
msgid "When do you want to stop playback?"
msgstr "Kad vēlaties apturēt atskaņošanu?"

#: ../src/uimisc.c:480
msgid "minutes"
msgstr "minūtes"

#: ../src/uimisc.c:488
msgid "Don't stop"
msgstr "Neapturēt"

#: ../src/uimisc.c:489
msgid "Stop after this track"
msgstr "Apturēt atskaņošanu pēc šī ieraksta"

#. Translators: the full text is "Stop after ____ minutes"
#: ../src/uimisc.c:491
msgid "Stop after"
msgstr "Apturēt pēc"

#: ../src/uimisc.h:20
msgid "_Recommend to..."
msgstr "_Rekomendēt..."

#: ../src/uimisc.h:21
msgid "_Tags..."
msgstr "_Iezīmes..."

#: ../src/uimisc.h:22
msgid "_Add to playlist"
msgstr "_Pievienot repertuāram"

#: ../src/uimisc.h:23
msgid "_Love this track"
msgstr "_Atzīmēt šo ierakstu kā mīļu"

#: ../src/uimisc.h:24
msgid "_Ban this track"
msgstr "Ai_zliegt šo ierakstu"

#. Configure widgets
#: ../src/vgl-bookmark-window.c:313 ../src/vgl-main-menu-hildon22.c:118
#: ../src/vgl-main-menu-hildon22.c:434
msgid "Bookmarks"
msgstr "Grāmatzīmes"

#. Last.fm
#: ../src/vgl-main-menu-gtk.c:196
msgid "_Last.fm"
msgstr "_Last.fm"

#: ../src/vgl-main-menu-gtk.c:200
msgid "_Import servers file..."
msgstr "_Importēt serveru failu..."

#. Radio
#: ../src/vgl-main-menu-gtk.c:219
msgid "Play _Radio"
msgstr "Spēlēt _radio"

#: ../src/vgl-main-menu-gtk.c:221
msgid "My radios"
msgstr "Mani radio"

#: ../src/vgl-main-menu-gtk.c:223
msgid "Others' radios"
msgstr "Citu radio"

#: ../src/vgl-main-menu-gtk.c:224 ../src/vgl-main-menu-hildon22.c:354
msgid "Group radio..."
msgstr "Grupu radio..."

#: ../src/vgl-main-menu-gtk.c:225 ../src/vgl-main-menu-gtk.c:293
#: ../src/vgl-main-menu-hildon22.c:352
msgid "Music tagged..."
msgstr "Mūzika ar tagiem..."

#: ../src/vgl-main-menu-gtk.c:226 ../src/vgl-main-menu-hildon22.c:353
msgid "Artists similar to..."
msgstr "Izpildītājam līdzīga mūzika..."

#: ../src/vgl-main-menu-gtk.c:227 ../src/vgl-main-menu-hildon22.c:362
msgid "Enter URL..."
msgstr "Ievadīt adresi..."

#: ../src/vgl-main-menu-gtk.c:248 ../src/vgl-main-menu-hildon22.c:355
msgid "My library"
msgstr "Mana kolekcija"

#: ../src/vgl-main-menu-gtk.c:249 ../src/vgl-main-menu-hildon22.c:356
msgid "My neighbours"
msgstr "Mani kaimiņi"

#: ../src/vgl-main-menu-gtk.c:250 ../src/vgl-main-menu-hildon22.c:357
msgid "My loved tracks"
msgstr "Mani iemīļotie ieraksti"

#: ../src/vgl-main-menu-gtk.c:251 ../src/vgl-main-menu-hildon22.c:358
msgid "My mix"
msgstr "Mans mikss"

#: ../src/vgl-main-menu-gtk.c:252 ../src/vgl-main-menu-hildon22.c:359
msgid "My playlist"
msgstr "Mans repertuārs"

#: ../src/vgl-main-menu-gtk.c:253 ../src/vgl-main-menu-hildon22.c:360
msgid "My recommendations"
msgstr "Manas rekomendācijas"

#: ../src/vgl-main-menu-gtk.c:254 ../src/vgl-main-menu-hildon22.c:361
msgid "My music tagged..."
msgstr "Mana mūzika ar tagiem..."

#: ../src/vgl-main-menu-gtk.c:287
msgid "Library..."
msgstr "Kolekcija..."

#: ../src/vgl-main-menu-gtk.c:288
msgid "Neighbours..."
msgstr "Kaimiņi..."

#: ../src/vgl-main-menu-gtk.c:289
msgid "Loved tracks..."
msgstr "Iemīļotie ieraksti..."

#: ../src/vgl-main-menu-gtk.c:290
msgid "Mix..."
msgstr "Mikss..."

#: ../src/vgl-main-menu-gtk.c:291
msgid "Playlist..."
msgstr "Repertuārs..."

#: ../src/vgl-main-menu-gtk.c:292
msgid "Recommendations..."
msgstr "Rekomendācijas..."

#. Actions
#: ../src/vgl-main-menu-gtk.c:324
msgid "_Actions"
msgstr "_Darbības"

#: ../src/vgl-main-menu-gtk.c:332 ../src/vgl-main-menu-hildon22.c:428
msgid "Stop after..."
msgstr "Apturēt pēc ..."

#: ../src/vgl-main-menu-gtk.c:340 ../src/vgl-main-window.c:105
msgid "Download this track"
msgstr "Lejupielādēt šo ierakstu"

#: ../src/vgl-main-menu-gtk.c:383
msgid "_Bookmarks"
msgstr "_Grāmatzīmes"

#: ../src/vgl-main-menu-gtk.c:386
msgid "Add bookmark..."
msgstr "Pievienot grāmatzīmi..."

#: ../src/vgl-main-menu-gtk.c:388 ../src/vgl-main-menu-hildon22.c:123
msgid "Manage bookmarks..."
msgstr "Pārvaldīt grāmatzīmes..."

#: ../src/vgl-main-menu-gtk.c:389 ../src/vgl-main-menu-hildon22.c:445
msgid "Bookmark this artist..."
msgstr "Pievienot šo izpildītāju grāmatzīmēm..."

#: ../src/vgl-main-menu-gtk.c:390 ../src/vgl-main-menu-hildon22.c:452
msgid "Bookmark this track..."
msgstr "Pievienot šo ierakstu grāmatzīmēm..."

#: ../src/vgl-main-menu-gtk.c:391 ../src/vgl-main-menu-hildon22.c:459
msgid "Bookmark this radio..."
msgstr "Pievienot šo radio grāmatzīmēm..."

#. Help
#: ../src/vgl-main-menu-gtk.c:417
msgid "_Help"
msgstr "_Palīdzība"

#: ../src/vgl-main-menu-hildon22.c:228 ../src/vgl-main-menu-hildon22.c:350
msgid "Select radio"
msgstr "Izvēlieties radio"

#: ../src/vgl-main-menu-hildon22.c:230
#, c-format
msgid "%s's library"
msgstr "%s kolekcija"

#: ../src/vgl-main-menu-hildon22.c:232
#, c-format
msgid "%s's neighbours"
msgstr "%s kaimiņi"

#: ../src/vgl-main-menu-hildon22.c:234
#, c-format
msgid "%s's loved tracks"
msgstr "%s iemīļotie ieraksti"

#: ../src/vgl-main-menu-hildon22.c:236
#, c-format
msgid "%s's mix"
msgstr "%s mikss"

#: ../src/vgl-main-menu-hildon22.c:238
#, c-format
msgid "%s's playlist"
msgstr "%s repertuārs"

#: ../src/vgl-main-menu-hildon22.c:240
#, c-format
msgid "%s's recommendations"
msgstr "%s rekomendācijas"

#: ../src/vgl-main-menu-hildon22.c:242
#, c-format
msgid "%s's music tagged..."
msgstr "%s mūzika ar tagiem..."

#: ../src/vgl-main-menu-hildon22.c:269
msgid "Type any user name"
msgstr "Ievadiet lietotājvārdu"

#: ../src/vgl-main-menu-hildon22.c:413
msgid "Import servers file"
msgstr "Importēt serveru failu"

#: ../src/vgl-main-menu-hildon22.c:418
msgid "Play radio"
msgstr "Spēlēt radio"

#: ../src/vgl-main-menu-hildon22.c:423
msgid "People"
msgstr "Lietotāji"

#: ../src/vgl-main-window.c:81
msgid "Start playing"
msgstr "Sākt spēlēt"

#: ../src/vgl-main-window.c:85
msgid "Stop playing"
msgstr "Apturēt"

#: ../src/vgl-main-window.c:89
msgid "Skip this track"
msgstr "Izlaist šo ierakstu"

#: ../src/vgl-main-window.c:93
msgid "Mark as loved"
msgstr "Atzīmēt kā mīļu"

#: ../src/vgl-main-window.c:97
msgid "Ban this track"
msgstr "Aizliegt šo ierakstu"

#: ../src/vgl-main-window.c:101
msgid "Recommend this track"
msgstr "Rekomendēt šo ierakstu"

#: ../src/vgl-main-window.c:109
msgid "Edit tags for this track"
msgstr "Labot tagus šim ierakstam"

#: ../src/vgl-main-window.c:113
msgid "Add this track to playlist"
msgstr "Pievienot šo ierakstu repertuāram"

#: ../src/vgl-main-window.c:197
msgid "<b>Artist:</b>\n"
msgstr "<b>Izpildītājs:</b>\n"

#: ../src/vgl-main-window.c:203
msgid "<b>Title:</b>\n"
msgstr "<b>Nosaukums:</b>\n"

#: ../src/vgl-main-window.c:210
msgid "<b>Album:</b>\n"
msgstr "<b>Albums:</b>\n"

#: ../src/vgl-main-window.c:219
msgid "<b>Listening to <i>"
msgstr "<b>Skan <i>"

#: ../src/vgl-main-window.c:348 ../src/vgl-main-window.c:349
msgid "Stopped"
msgstr "Apturēts"

#: ../src/vgl-main-window.c:373
msgid "Playing..."
msgstr "Spēlē..."

#: ../src/vgl-main-window.c:388 ../src/vgl-main-window.c:389
msgid "Connecting..."
msgstr "Pieslēdzas..."

#: ../src/vgl-main-window.c:408
msgid "Disconnected"
msgstr "Atvienots"

#: ../src/vgl-main-window.c:486
#, c-format
msgid "Volume: %d/100"
msgstr "Skaļums: %d/100"

#: ../src/vgl-sb-plugin.c:34
msgid "No artist"
msgstr "Nav izpildītāja"

#: ../src/vgl-sb-plugin.c:35
msgid "No track"
msgstr "Nav ieraksta"

#: ../src/vgl-sb-plugin.c:36
msgid "No album"
msgstr "Nav albuma"

#: ../src/vgl-sb-plugin.c:38
msgid "Play"
msgstr "Atskaņot"

#: ../src/vgl-sb-plugin.c:39
msgid "Stop"
msgstr "Apturēt"

#: ../src/vgl-sb-plugin.c:40
msgid "Skip"
msgstr "Izlaist"

#: ../src/vgl-sb-plugin.c:41
msgid "Love"
msgstr "Mīļākā dziesma"

#: ../src/vgl-sb-plugin.c:42
msgid "Ban"
msgstr "Aizliegt"

#: ../src/vgl-sb-plugin.c:43
msgid "Show main window"
msgstr "Parādīt galveno logu"

#: ../src/vgl-sb-plugin.c:44
msgid "Close Vagalume"
msgstr "Aizvērt Vagalume"

#: ../src/vgl-tray-icon.c:23
msgid " Stopped "
msgstr "Apturēts"

#: ../src/vgl-tray-icon.c:24
#, c-format
msgid ""
" Now playing: \n"
" %s \n"
"   by  %s "
msgstr ""
" Šobrīd spēlē: \n"
" %s \n"
"   no  %s  "

#: ../src/vgl-tray-icon.c:26
#, c-format
msgid " by <i>%s</i>"
msgstr " no <i>%s</i>"

#: ../src/vgl-tray-icon.c:27
#, c-format
msgid ""
" by <i>%s</i>\n"
" from <i>%s</i>"
msgstr ""
" no <i>%s</i>\n"
" no <i>%s</i>"

#~ msgid ""
#~ "* Use HTTP proxy:\n"
#~ "Enable this to use an HTTP proxy.\n"
#~ "\n"
#~ "* Proxy address:\n"
#~ "user:password@hostname:port\n"
#~ "Only the hostname (can be an IP address) is required."
#~ msgstr ""
#~ "* Lietot HTTP starpniekserveri:\n"
#~ "Aktivizējiet, lai lietotu HTTP starpniekserveri.\n"
#~ "\n"
#~ "* Starpniekservera adrese:\n"
#~ "lietotājvārds:parole@adrese:ports\n"
#~ "Noteikti nepieciešama tikai adrese (var būt IP adrese)."

#~ msgid "Stop after ..."
#~ msgstr "Apturēt pēc ..."

#~ msgid "Invalid radio URL"
#~ msgstr "Nederīga radio adrese"

#~ msgid "A Last.fm client for Gnome and Maemo"
#~ msgstr "Last.fm klients Gnome un Maemo vidēm"

#~ msgid "Settings..."
#~ msgstr "Iestatījumi..."

#~ msgid "Quit"
#~ msgstr "Iziet"

#~ msgid "About..."
#~ msgstr "Par..."
